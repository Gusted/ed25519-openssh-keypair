# Create ed25519 OpenSSH keys

Generate a keypair for the [ed25519](https://ed25519.cr.yp.to/) elliptic curve and transform them into OpenSSH-compatible format.

Licensed by MIT License, feel free to take the code and adopt to your needs.
